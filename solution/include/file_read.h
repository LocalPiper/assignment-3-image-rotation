#pragma once
#include "image.h"
#include "read_status.h"
extern enum read_status from_bmp(const char *filename,
                                 struct image **destination);
