#include "../include/file_read.h"
#include "../include/bmp_img.h"
#include "../include/image.h"
#include "../include/image_assembly.h"
#include "../include/padding.h"
#include "../include/utils.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct image *read_image(FILE *f, const uint32_t width, const uint32_t height) {
  uint8_t padding = calculate_padding(width);

  struct image *image = create_blank_image(width, height);

  for (uint32_t i = 0; i < height; ++i) {
    if (fread(&image->data[i * width], sizeof(struct pixel), width, f) !=
        width) {
      print_error("READ ERROR");
      free(image->data);
      return NULL;
    }
    fseek(f, padding, SEEK_CUR);
  }
  return image;
}

enum read_status read_header_and_image(FILE *f, struct image **image) {
  struct bmp_header header;
  if (fread(&header, sizeof(struct bmp_header), 1, f) != 1) {
    print_error("READ INVALID HEADER");
    return READ_INVALID_HEADER;
  }
  if (header.bfType != SIGNATURE) {
    print_error("READ INVALID SIGNATURE");
    return READ_INVALID_SIGNATURE;
  }
  if (header.biBitCount != BIT_COUNT) {
    print_error("READ INVALID BITS");
    return READ_INVALID_BITS;
  }
  if (header.biCompression != COMPRESSION_TYPE) {
    print_error("READ INVALID COMPRESSION");
    return READ_INVALID_COMPRESSION;
  }
  *image = read_image(f, header.biWidth, header.biHeight);

  return READ_OK;
}

enum read_status from_bmp(const char *filename, struct image **destination) {
  FILE *f = fopen(filename, "rb");
  if (!f) {
    print_error("Error while opening file to read");
    return READ_OPEN_ERROR;
  }

  read_header_and_image(f, destination);
  fclose(f);
  return READ_OK;
}
