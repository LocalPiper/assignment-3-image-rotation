#include "../include/image_assembly.h"
#include "../include/bmp_img.h"
#include "../include/image.h"
#include "../include/padding.h"
#include "../include/read_status.h"
#include "../include/utils.h"
#include <stdint.h>
#include <stdlib.h>

struct pixel *generate_empty_pixels(struct pixel *storage, uint32_t width,
                                    uint32_t height) {
  for (uint32_t i = 0; i < (height * width); ++i) {
    storage[i].b = 0;
    storage[i].g = 0;
    storage[i].r = 0;
  }
  return storage;
}

struct image *create_blank_image(uint32_t width, uint32_t height) {
  struct image *empty_image = (struct image *)malloc(sizeof(struct image));
  empty_image->width = width;
  empty_image->height = height;
  empty_image->data =
      (struct pixel *)malloc(height * width * sizeof(struct pixel));
  empty_image->data = generate_empty_pixels(empty_image->data, width, height);
  return empty_image;
}
