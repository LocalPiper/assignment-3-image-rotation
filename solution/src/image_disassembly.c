#include "../include/image_disassembly.h"
#include "../include/bmp_img.h"
#include "../include/padding.h"
#include "../include/utils.h"
#include "../include/write_status.h"
#include <stdint.h>
#include <stdio.h>

enum write_status put_header(FILE *f, struct bmp_header *header) {
  if (fwrite(header, sizeof(struct bmp_header), 1, f) != 1) {
    print_error("Error writing header");
    return WRITE_ERROR;
  }

  return WRITE_OK;
}

enum write_status put_image(FILE *f, struct image const *source,
                            uint8_t padding) {
  uint8_t padding_array[3] = {0};
  for (uint32_t i = 0; i < source->height; i++) {
    if (fwrite(&source->data[i * source->width], sizeof(struct pixel),
               source->width, f) != source->width) {
      print_error("Error writing data");
      return WRITE_ERROR;
    }

    if (padding > 0) {
      if (fwrite(padding_array, 1, padding, f) != padding) {
        print_error("Error writing padding");
        return WRITE_ERROR;
      }
    }
  }

  return WRITE_OK;
}

void write_image(FILE *f, struct image **source) {

  uint8_t padding = calculate_padding((*source)->width);
  struct bmp_header header = create_header(*source, padding);

  if (put_header(f, &header) != WRITE_OK) {
    return;
  }

  put_image(f, *source, padding);
}
