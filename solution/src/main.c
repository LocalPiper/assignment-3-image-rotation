#include "../include/file_read.h"
#include "../include/file_write.h"
#include "../include/transform.h"
#include "../include/utils.h"
#include "../include/validate.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
  // check if args are valid
  if (validate(argc, argv) == 1) {
    printf("%s", "Program terminated.");
    return 1; // Return error code
  }

  const char *source_filename = argv[1];
  const char *destination_filename = argv[2];
  const int16_t turns = parse_angle(argv[3]);

  // get image object from file
  struct image *image;
  if (from_bmp(source_filename, &image) != READ_OK) {
    return 1;
  }

  // rotate image
  transform(&image, turns);

  // shove image into a file
  if (to_bmp(destination_filename, &image) != WRITE_OK) {
    return 1; // Return error code
  }
  free(image->data);
  free(image);

  // and we are good to go
  return 0;
}
