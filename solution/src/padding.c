#include "../include/padding.h"
#include "../include/pixel.h"
#define C_PADDING 4
uint8_t calculate_padding(uint32_t width) {
  return (C_PADDING - width * sizeof(struct pixel) % C_PADDING) % C_PADDING;
}
