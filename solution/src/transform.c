#include "../include/image.h"
#include <stdint.h>
#include <stdlib.h>
void swap_parameters(struct image *source_img) {
  uint32_t temp = source_img->width;
  source_img->width = source_img->height;
  source_img->height = temp;
}
// rotates image by 90 degrees (bruh)
void rotate(struct image *source_img) {
  struct pixel *transformed_data =
      malloc(source_img->height * source_img->width * sizeof(struct pixel));
  for (size_t col = 0; col < source_img->height; ++col) {
    // first, we transpose row of source_img into column of transformed_data
    for (size_t row = 0; row < source_img->width; ++row) {
      transformed_data[row * source_img->height + col] =
          source_img->data[col * source_img->width + row];
    }
    // then, we mirror transposed data
    for (size_t row = 0; row < source_img->width / 2; ++row) {
      struct pixel temp = transformed_data[row * source_img->height + col];
      transformed_data[row * source_img->height + col] =
          transformed_data[(source_img->width - row - 1) * source_img->height +
                           col];
      transformed_data[(source_img->width - row - 1) * source_img->height +
                       col] = temp;
    }
    // it may look complicated and unoptimal being O(n^2), but it gets the job
    // done
  }
  free(source_img->data);
  source_img->data = transformed_data;
  swap_parameters(source_img);
}
void transform(struct image **source_img, int16_t turns) {
  for (long i = 0; i < turns; ++i) {
    rotate(*source_img);
  }
}
