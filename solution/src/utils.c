#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
void print_error(const char *message) {
  fputs(message, stderr);
  fputs("\n", stderr);
}
int file_exists(const char *filename) { return access(filename, F_OK) == 0; }

int16_t parse_angle(const char *str_angle) {
  int16_t angle = (int16_t)atoi(str_angle);
  int16_t module = (int16_t)abs(angle);
  if (angle != module) {
    return (int16_t)(4 - module / 90);
  }
  return (int16_t)(module / 90);
}
