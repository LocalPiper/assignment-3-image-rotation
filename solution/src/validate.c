#include "../include/utils.h"
#include <stdio.h>
int validate(int arg_count, char **args) {
  // check number of args
  if (arg_count != 4) {
    fprintf(stderr, "ERROR: Invalid number of arguments, expected 3, got %d",
            arg_count - 1);
    return 1;
  }
  // check file path
  if (!file_exists(args[1])) {
    print_error("ERROR: Invalid filename");
    return 1;
  }

  return 0;
}
